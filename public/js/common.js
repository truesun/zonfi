var mbW = 767;
$(document).ready(function () {
    //=== cookie ===
    var cookieShow = $('#cookieBox').is(':visible');
    if (!cookieShow) {
        $('#sideNavArea').addClass('nocookie');
        $('#bannerArea').addClass('nocookie');
    }
    $('#cookieAgree').on('click', function () {
        $('#cookieBox').hide();
        $('#sideNavArea').addClass('nocookie');
        $('#bannerArea').addClass('nocookie');
        $('nav').addClass('nocookie');
        //=== if cookie unfix ===
        // $('nav,#sideNavArea').removeAttr('style');
    });
    //=== 側邊nav ===
    $('#sideNavOpen').on('click', function () {
        var isShow = $('#sideNavArea').hasClass('on');
        if (isShow) {
            $(this).removeClass('sideAct');
            $('#sideNavArea').removeClass('on');
            $('#navMask').fadeOut();
            bgUnFix();
        } else {
            $(this).addClass('sideAct');
            $('#sideNavArea').addClass('on');
            $('#navMask').fadeIn();
            bgFix();
        }
    });
    // $('#sideNavClose').on('click', function () {
    //     $('#sideNavArea').removeClass('on');
    //     $('#navMask').fadeOut();
    //     bgUnFixPc();
    // });
    //=== nav click ===
    $('.nav-box-tit').on('click', function () {
        var $cont = $(this).next();
        var isShow = $cont.is(':visible');
        if (isShow) {
            $cont.slideUp();
            $(this).removeClass('on');
        } else {
            $('.nav-box-tit').removeClass('on');
            $('.nav-box-sub').slideUp();
            $(this).addClass('on');
            $cont.slideDown();
        }
    });

    //=== footer 跑馬燈 ===
    marqueeMesFun();
    //=== footer mb social click ===
    $('.f-mb-tit').on('click', function () {
        var windowW = $(window).width();
        if (windowW < mbW) {
            var $cont = $(this).next();
            var isShow = $cont.is(':visible');
            isShow ? $cont.slideUp() : $cont.slideDown();
        }
    });

    //=== 選單 和 footer 點擊滑動 ===
    var slideToList = [
        { 'navBtn': '#basicAreaNavBtn1', 'fBtn': '', 'mbBtn': '', 'cont': '#basicArea1' },
        { 'navBtn': '#basicAreaNavBtn2', 'fBtn': '', 'mbBtn': '', 'cont': '#basicArea2' },
        { 'navBtn': '#basicAreaNavBtn3', 'fBtn': '', 'mbBtn': '', 'cont': '#basicArea3' },
        { 'navBtn': '#navAreaBtn1', 'fBtn': '#fAreaBtn1', 'mbBtn': '#areaMbBtn1', 'cont': '#area1' },
        { 'navBtn': '#navAreaBtn2', 'fBtn': '#fAreaBtn2', 'mbBtn': '#areaMbBtn2', 'cont': '#area2' },
        { 'navBtn': '#navAreaBtn3', 'fBtn': '#fAreaBtn3', 'mbBtn': '#areaMbBtn3', 'cont': '#area3' }
    ];
    $.each(slideToList, function (ind, val) {
        //== nav ==
        var $navBtn = $(val.navBtn);
        var $cont = $(val.cont);
        $navBtn.on('click', function () {
            $('#sideNavOpen').removeClass('sideAct');
            $('#sideNavArea').removeClass('on');
            $('#navMask').fadeOut();
            bgUnFix();

            $('html,body').stop().animate({ scrollTop: $cont.offset().top }, 'slow');
        });
        //== footer ==
        if (val.fBtn) {
            var $fBtn = $(val.fBtn);
            $fBtn.on('click', function () {
                $('html,body').stop().animate({ scrollTop: $cont.offset().top }, 'slow');
            });
        }
        //== mb about ==
        if (val.mbBtn) {
            var $mbBtn = $(val.mbBtn);
            $mbBtn.on('click', function () {
                $('html,body').stop().animate({ scrollTop: $cont.offset().top }, 'slow');
            });
        }
    });
});

$(window).scroll(function () {
    var windowScrollNum = document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop;
    //=== if cookie unfix ===
    // var cookieShow = $('#cookieBox').is(':visible');
    // if (cookieShow) {
    //     if (windowScrollNum < 50) {
    //         var topMar = 50 - windowScrollNum;
    //         // console.log(topMar);
    //         $('nav').css('top', topMar + 'px');
    //         $('#sideNavArea').css('height', 'calc(100vh - ' + topMar + 'px)');
    //     } else {
    //         $('nav').css('top', '0px');
    //         $('#sideNavArea').css('height', '100vh');
    //     }
    //     // $('#bannerArea').css('height', 'calc(100vh - ' + topMar + 'px)');
    // }
    //=== logo縮放 ===
    if (windowScrollNum > 50) {
        $('#logo').addClass('on');
        $('nav').addClass('bg');
        $('#bannerTxtArea').fadeOut();
    } else {
        $('#logo').removeClass('on');
        $('nav').removeClass('bg');
        $('#bannerTxtArea').fadeIn();
    }
    //=== 前言logo ===
    var windowH = $(window).height();
    // var sloganH = $('#sloganArea').offset().top;
    var sloganLogoH = $('#sloganLogo').offset().top;
    var basicArea1H = $('#basicArea1').offset().top;
    // console.log('windowH', windowH);
    // console.log(windowScrollNum);
    // console.log('logo', sloganLogoH);
    // console.log('area', basicArea1H);
    var logoStartLine = sloganLogoH - windowH;//開始線
    // basicArea1H == endLine
    var transItemH = basicArea1H - logoStartLine;//作用總區間高度
    if (windowScrollNum > logoStartLine && windowScrollNum < basicArea1H) {
        // console.log('start');
        var transH = windowScrollNum - logoStartLine;//開始線後長出的高
        // console.log('1/', transH);
        var tPlus = (transH / transItemH) * 0.5;//要放大的最大數
        // console.log(tPlus);
        var tPlusNum = 1 + tPlus;//1.5
        $('#sloganLogo').find('img').css({ 'transform': 'scale(' + tPlusNum + ')' });
        // 100/1000 = X/250 , X = (100/1000)*250;
        // var videoTrmY = (plusH / part1L) * 250 - 250;//可以改變滾動的距離(250)
        // console.log(tPlus);
    }



});
//=== footer 跑馬燈 ===
function marqueeMesFun() {
    //跑馬燈
    var Speed = 15;
    var timer;
    var cont1W = Math.floor($('#marqueeBox1').outerWidth());
    var cont2pL = Math.floor($('#marqueeBox2').position().left) - cont1W;
    //利用這裡的外層盒子跑 scrollLeft ， 注意 #marqueeWrap 需要下 overflow:hidden 才會跑
    $('#marqueeWrap').stop().animate({ scrollLeft: 0 }, 6000);
    // == 向左跑 ==
    function picMarquee() {
        //若pic2的寬小於myDiv的scrollLeft，表示捲軸已經過了第一行
        //所以讓Div的scrollLeft再回到原來的位置；反之 則繼續往右跑
        if (cont1W - cont2pL <= 0) {
            $('#marqueeWrap').stop().animate({ scrollLeft: 0 }, 0);
            cont2pL = 0;
        } else {
            $('#marqueeWrap').stop().animate({ scrollLeft: cont2pL++ }, 0);
        }
    }
    timer = setInterval(picMarquee, Speed);
    //- timer = setInterval(picMarqueeR, Speed); //向右跑
    // 當滑鼠在myDiv上時 就停止捲動
    // $('#marqueeWrap').on('mouseenter', function () {
    //     clearInterval(timer);
    // });
    // // //當滑鼠離開myDiv上時 就繼續往右捲動
    // $('#marqueeWrap').on('mouseleave', function () {
    //     timer = setInterval(picMarquee, Speed);
    // });
}

function bgFix() {
    $('body').css({ 'overflow': 'hidden' });
}
function bgUnFix() {
    $('body').removeAttr('style');
}